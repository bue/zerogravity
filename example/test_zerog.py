###
# title test_zerog.py
#
# language: python3.6
# license: GPL >= 3
# date: 2017-05
# author: bue
#
# run:
#     python3 test_zerog.py
#
# output:
#     + the Bagel_and_Blue_Chees_{OS} rendered game executable
#       for your operating system,
#     + the Bagel_and_Blue_Chees_code_{OS}.py source code which was
#       run by the dockerized blender to render the game executable.
#     + the Bagel_and_Blue_Chees_Blender game executable which can be
#       run and furter developend inside blender.
#
# description:
#     this code serves as example and test code for your
#     zerogravity installation.
#     zerogravity can be installed via pip.
#     source code: https://gitlab.com/biotransistor/zerogravity
#     user manual: https://zerogravity.readthedocs.io/en/latest/
####

import zerogravity.zerog as g

# const
tr_BLACK = (0, 0, 0)
tr_BLUE = (0, 0, 1)
tr_CYAN = (0, 0.5, 0.5)
tr_GREEN = (0, 1, 0)
tr_WHITE = (1, 1, 1)
tr_YELLOW = (0.5, 0.5, 0)
tr_ORANGE = (1, 0.32, 0.04)
tr_RED = (1, 0, 0)
tr_BROWN = (0.64, 0.32, 0.04)

# get blender
o_zerog = g.zeroGblender()

# add bagel and blue chees meshes
o_torus = g.Mesh(
    relative_path_file="stl/torus.stl",
    diffuse_color=tr_BROWN,
    visibility_key="NINE"
)
o_zerog.add(o_torus)

o_grid = g.Mesh(
    relative_path_file="stl/grid.stl",
    diffuse_color=tr_CYAN
)
o_zerog.add(o_grid)

# add keyboard navigation
o_navik = g.KeyboardNavi()
o_zerog.add(o_navik)

# add tourch
o_torch = g.Torch()
o_zerog.add(o_torch)

# add global position system
o_gps = g.Gps(key="ONE")
o_zerog.add(o_gps)

# add gamepad navigation
#o_navig = g.GamepadNavie()
#o_zerog.add(o_navig)

# add head mounted display lense distortion
o_distortion = g.HmdLenseDistortion()
o_zerog.add(o_distortion)

# zerogmodules not added in this test case
# + Code
# + Scene

# render game for a specific os
#o_zerog.zerog_os = "Windows"
#o_zerog.blend_game(b_debug=True)

#o_zerog.zerog_os = "Darwin"
#o_zerog.blend_game(b_debug=True)

#o_zerog.zerog_os = "Linux"
#o_zerog.blend_game(b_debug=True)

# render game for host os
o_zerog.blend_game("Bagel and Blue Chees", b_debug=True)
