# Discussion

<!--
idee: microscopy z stack images.
would be nice to study them withj vr.
a poormans cave.
a cave for every one.
a cave for a hadn full dollars.
an opensource implementation.
fisrt experiance to belnder at the pycon in montereal
Blendewr ist the only free and opensource game engine i am aware of.
So, it is definitely worth diggin into it.
Free and opensource alternatives are important for research.
-->


## Contribution

+ Implementation: Elmar Bucher
+ User Manual: Elmar Bucher
+ Concept: Kevin Loftis, Elmar Bucher
+ Supervision: Danielle Jorgens


The authors like to say thank you to:

+ Jonathan Rogers for ledning out his Occulus Rift.
+ Lubosz Sarnecki for figuring out how to conect as VR Head Mounted Display to
  Blender and writing a bpy lense distortion code that is used in zerogravity.
+ Blender PDX for an informative Saturday afternoon.
+ Monster from the blende community who aswered a lot of my questions
  in th forums.
+ Jenny Cheng for the "Intro to 3D Graphics with Blender and the Blender API"
  tutorial at PyCon2015 in Montreal, Canada.

Zerogravito stands on ther shoulder of giants!

+ Python3
+ Blender
+ noVnc
+ Docker
+ Debian Gnu/Linux

Without them this implementation would never have been possible.
