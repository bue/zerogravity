# HowTos  ZeroGravity

## Howto install zerogravity

1. If not already installed, install [Docker Engine](https://docs.docker.com/)
    as described here: [Install Docker](https://docs.docker.com/install/)
    How depends on the flavor of your operating system.

1. The zerogravity library it self can be installed with pip.
    ```{bash}
    pip3 install zerogravity
    ```
    zerogravity shoudl install the library in the `site-package` folder
    and an `zerogravity_example` folder in your home directory.


1.  From your home directory run
    ```{bash}
    python3 zerogravity_example/test_zerog.py
    ```
    You have to have a working internet connection!
    It will take a long time to complete this test run.
    Please be patient.
    This is what happens in the back ground:

    + pulling the lates debian:sid docker image from docker hub.
    + building based on the debian image the zerogravity image,
      which contains a linux based, headless blender installation.

    Then zerogravity will complain that no blenderplayer directory
    for the latest belnder version has been found.
    Do as you prompted to install the belnderplayer of Windows and Mac OSX!

1. Finnaly, run again
    ```{bash}
    python3 example/test_zerog.py
    ```
    You do not have to have a working internet connection.
    This time no error should occure.
    And the test run should pass much faster,
    because an up to data zerogravity docker image should already exist.
    Thus, no debian docker container have to be pulled
    and no zerogravity docker image havd to be built.
    This is what happens in the back ground:

    + buildiung a zerogravity container instance out of the zerogravity image.
    + render the game as specified in the example/test_zerog.py file.
    + destoy the container.


## Howto totally uninstall zerogravity

1. Open a python shell and execute the following commands.
    ```{python}
    import zerogravity
    zerogravity
    ```
    From the displayed zerogravity module path
    delete the `zerogravity/bplayer` folder!

1. Run
    ```{bash}
    pip3 uninstall zerogravity, docker, requests, setuptools
    ```

1. Uninstall Docker as described on their [hompage](https://docs.docker.com/).
    How depends on the flavor of your operating system and how you installed it.

1. In your home directory delete the `zerogravity_example` folder.
   ```{bash}
   cd ~
   rm -r zerogravity_example
   ```

## Howto only unistall the zerogravity python library

Follow this steps will only uninstall the python zerogravity libarary
and let the docker installation and
python docker, request and setuptools library untouched.

1. Open a python shell and execute the following commands.
  ```{python}
  import zerogravity
  zerogravity
  ```
  From the displayed zerogravity module path
  delete the `zerogravity/bplayer` folder!

1. Run
    ```{bash}
    pip3 uninstall zerogravity
    ```


## Howto render a blender game executable

Open a python3 shell or write and run a python3 script with the following code:

```
import zerogravity.zerog as g  # import zerogravity library

o_zerog = g.zeroGblender()  # make a zerogravity instance
o_zerog.blend_game() # render zerogravity instance into game executable
```
This game executable have nothing gamable in int.
To make it gameble you have to `add` at least a Mesh or 3D object
and game controll like KeyboardNavi, Gps, Tourch or GamepadNavi

bue: mention that you can render for othe r os then the os on the host computer (default)!
bue: how to give a game name and what the default is
bue: for detail description run `o_zerog.blend_game?` to read the doc string
    or look up the commond in the reference.

## Howto add a 3D object to a zerogravity instance

1. generate a zero gavity instance as described in
    `Howto render a belnder game executable`.
2. assuming your zerogravity instance named is o_zerog
    run the follwong code before you render the game out of the instance.

2a. for a mesh object like ply or stl:
  ```
  o_mesh = g.Mesh(relative_path_file="path/to/mesh.stl")
  o_zerog.add(o_mesh)
  ```
bue: for detail description run `g.Mesh?` to read the doc string.
    or look up the commond in the reference.


2b. for a scene object like 3ds, fbx, obj, wrl or x3d:
  ```
  o_scene = g.Scene(relative_path_file="path/to/scene.x3d")
  o_zerog.add(o_scene))
  ```
bue: for detail description run `g.Scene?` to read the doc string.
    or look up the commond in the reference.


## Howto add game control to a zerogravity instance

game controll can modularly be added to the game.
if you have a game pad
if you just

if you like, you can add all avilabe game controll (the gamepad and the keabor based).

KeyboardNavi
Torch
Gps
GamepadNavi

## Howto add blender python code to a zerogravity instance


## Howto write a new zerogravity module

You will have to work with a local Blender installation.
Please download and install the latest version from
[the official Blender homepage](https://www.blender.org/).

Mastering Blender, the GUI and the API, is a challenge for it self.
If you are new to Blender, then I would start with the
official Referenc Manual, especially when your main intrest is the game engine
and not animation as such.
Then I would digg into the official Blender Python API manual.

This listed resources where helpfull for me:

+ The official [Blender Refence Manual](https://docs.blender.org/manual/en/dev/index.html).
    Particularely the game engine chapter
+ The official [Blender API documentation](https://docs.blender.org/api/blender_python_api_current/).
+ The pakerback book [Game development with Belnder](http://www.dalaifelinto.com/?page_id=919)
    from Dalai Felinto and Mike Pan.


This is how I usually set up my developer environment.

1. Open Blender via command line!
   ```{bash}
   blender
   ```
   This is important. Output form your `Python Controllers` while
   testing the game inside Blender (either by pressing `P` in `3D View`
   or starting the `Embeded Player` in the `Properties` editor)
   will only be visible in this console. When you open Blender
   simply by double click you will not know what is going on.

1. At the `Info` menu bar on the top change in the engine dropdown
    from `Blender Render` to `Blender Game`.

1. At the top left window you can let the `3D View` as it is .

1. At the bottom left window `Logic Editor`

1. At the top right window choose `Text Editor`.
    Once the Text Editor is up select `Show line numbers next to the text`
    and `Syntax highliting for scripting`.

1. At the bottom right window choose `Python Console`.
   Be aware that in this console autocompletation works with `Ctrl + Spacebar`,
   and not with `Tab` as you might be used to.
   And there is no way to change this behavior - which is most annoying!

1. When I have to use an other editor like `Outliner` or `Properties`
   then I usualy change my top right windows with the `Text editor`.
   The `Text editor` I use the least.

In Blender window `focus follows mouse`!
This is like the old Unix X Window System X11 style.
So, for example your mouse have to hover over the Python Console
to be able to inteact with it.

Some words to the `bpy` blender python library:
`import bpy` works only in this `Console` inside Blender,
or in `Scripts` that you write or load and run inside this `Text Editor` inside Blender,
or in `Scripts` or `Modules` that you load (e.g. via in the `Logical Editor`)
into a `Python Controller`,
or in code hat you send via command line command `blender --python mycode.py` to blender.
There is now way to install `bpy` outside this Blender development environment.

Some word to the `bge` blender game engine library:
`import bge` works only in `Scripts` or `Modules` that you load into a
`Python Controller` and now where else.
There is now way to install `bge` outside this blender development environment.

Now you are ready to develop.

1. To get an understaning how modules for zerogravity are written follow
    the tutorial from this documentation, then take a look at the resulting
    `Bagel_and_Blue_Chees_code_{OS}.py` file. This is the file that is sent
    via `command line` to the dockerized headless blender to render
    the corresponding game.

1. Now take a look at zerogravity's sourcecode. The `bgectrlrmodule.py` file
    contains `Module` code that is loaded into the `Python Controllers`.

1. In the `zerogmodule.py` file you see **how zerogravity modules are written**.
    You will realize that for each module is a Python `class`.
    The code that will be executed by Blender Python is provided as a
    list of strings (`ls_code`).
    Import commands for python libraries needed in `ls_code` have to be
    specified in a set of stings (`es_import`).
    Finllay `es_import` and `ls_code` are packed into a  dictionary (`d_code`).
    This dictionary `self.ds_code` is the standard output from each and every
    zerogravity module.

1. Now have a look at the `zerog.py`. This is zerogravitie's main program
    with the zeroGblender class. Via the `__init__.py` function this
    main program takes care that a headles dockerized blender container exist.
    Further provides this main program the `add` function to put a game out of
    zerogravity modules together.
    And the `blend_game` function which typeset the zerogravity module based
    Blender Python code to code for an entier game together
    (for example `Bagel_and_Blue_Chees_code_{OS}.py`) and send it
    to the blender container to get the game rendered.
