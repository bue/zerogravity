# Tutorial

In this tutoreal you will guide you step by step though the development of the
zerogravity_example/test_zerog.py Bagel and Blue Cheese game.

1. Install zerogravity as described in `Howto install zerogravity` section
    of this user manual.

## Initiate a zerogravity blender object

## Add 3D objects

## Add game control

## Add additional blender python code

## Rendering games
